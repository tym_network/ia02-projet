% % % % % %
% MINMAX  %
% % % % % %

% Retourne le minimum entre MV1 et MV2 dans MV et retourne le meilleur coup associ� dans MC (+MC1 : coup1, +MV1 : �valuation de MC1, +MC2 : coup2, +MV2 : �valuation de MC2, -MC : meilleur coup, -MV : valeur min entre MV1 et MV2)
min(MC1,MV1,MC2,MV2,MC,MV):-MV1>MV2 -> MC=MC2, MV=MV2; MC=MC1, MV=MV1.

% Retourne le maximum entre MV1 et MV2 dans MV et retourne le meilleur coup associ� dans MC (+MC1 : coup1, +MV1 : �valuation de MC1, +MC2 : coup2, +MV2 : �valuation de MC2, -MC : meilleur coup, -MV : valeur max entre MV1 et MV2)
max(MC1,MV1,MC2,MV2,MC,MV):-MV1<MV2 -> MC=MC2, MV=MV2; MC=MC1, MV=MV1.

% Vrai sil n'y a plus de coups jouables (+Plateau, +Der_tuile : derni�re tuile jou�e)
feuille(Plateau,Der_tuile):-liste_coups_possibles(Plateau,Der_tuile,[]).

% Retourne le meilleur coup � jouer par l'IA � l'aide de l'algorithme MinMax (+Plateau, +DerT : derni�re tuile jou�e, +Joueur, +Prof : profondeur d'exploration, -MC : Meilleure tuile, - MV : valeur de la meilleure tuile)
meilleur_coup(Plateau, DerT, Joueur, Coup):-meilleur_coup(Plateau, DerT, Joueur, 0, Coup, _).
meilleur_coup(Plateau, DerT, Joueur, Prof, MC, MV):-liste_coups_possibles(Plateau,DerT,ListeCoups), Prof2 is Prof+1,min_max(Plateau, Joueur, Prof2, ListeCoups, MC, MV).

% Algorithme MinMax (+Plateau, +DerT : derni�re tuile jou�e, +Joueur, +Prof : profondeur d'exploration, +ListeCoups : liste des coups possibles pour cette profondeur, -MC : Meilleure tuile, - MV : valeur de la meilleure tuile)
min_max(Plateau, Joueur, Prof, [CP1|RCP1], MC, MV):-jouer_coup(Plateau, [Joueur|[CP1]], NP),
	(feuille(NP,CP1) -> 
		%Si NP est une feuille
		score_net(NP,Joueur,CP1,Score), 
			(RCP1=[] -> 
				%Il n'y a pas d'autre coup
				MC=CP1, MV=Score;
				min_max(Plateau, Joueur, Prof, RCP1, MC2, MV2), P is Prof mod 2, 
					(P=:=0 -> 
						max(CP1,Score,MC2,MV2,MC,MV); % Prof mod 2 == 0 ; �tage max
						min(CP1,Score,MC2,MV2,MC,MV) % Prof mod 2 == 1 ; �tage min
					)
			);
		%Si NP n'est pas une feuille 
		(Prof>2 ->
			% Si on atteint la profondeur max d'exploration
			score_net(NP,Joueur,CP1,Score), 
				(RCP1=[] -> 
					MC=CP1, MV=Score;
					min_max(Plateau, Joueur, Prof, RCP1, MC2, MV2), P is Prof mod 2, 
						(P=:=0 -> 
							max(CP1,Score,MC2,MV2,MC,MV); 
							min(CP1,Score,MC2,MV2,MC,MV)
						)
				);
			autre_joueur(Joueur, J2), meilleur_coup(NP, CP1, J2, Prof, _, MV1), 
				(RCP1=[] -> 
					MC=CP1, MV=MV1;
					min_max(Plateau, Joueur, Prof, RCP1, MC2, MV2), P is Prof mod 2, 
						(P=:=0 -> 
							max(CP1,MV1,MC2,MV2,MC,MV); 
							min(CP1,MV1,MC2,MV2,MC,MV)
						)
				)
		)
	).
