% % % %
% JEU %
% % % %

% Retire une tuile aléatoire d'une liste. Elle retourne la tuile ainsi que la nouvelle liste sans cette tuile (+Liste_tuile : Liste initiale, -Tuile : tuile tirée, -Nouv_liste : la liste initiale sans la tuile tirée)
tirer_tuile(Liste_tuile,Tuile,Nouv_liste):-length(Liste_tuile,L), L_prime is L+1, randomize, random(1, L_prime, R),nth(R,Liste_tuile,Tuile),delete(Liste_tuile,Tuile,Nouv_liste).

% Retire quatre tuiles aléatoirement d'une liste et les insère dans une nouvelle liste (+Liste_tuile : liste initiale, - Liste_sortie : Liste des quatre tuiles tirées, -Nouv_liste : liste initiale sans les tuiles tirées)
tirer_ligne(Liste_tuile, Ligne_sortie, Nouv_liste):-tirer_ligne(Liste_tuile, Ligne_sortie, Nouv_liste, 0).
tirer_ligne(Liste_tuile, Ligne_sortie, Liste_tuile, N):- N>=4, Ligne_sortie = [], !.
tirer_ligne(Liste_tuile, Ligne_sortie, Nouv_liste, N):- N<4, N_prime is N+1, tirer_tuile(Liste_tuile, Nouv_tuile, Return_liste), tirer_ligne(Return_liste, X, R, N_prime), Ligne_sortie = [Nouv_tuile|X], Nouv_liste = R.

% Génère un plateau 4*4 à partir d'une liste de tuiles (+Liste_tuile : liste initiale, - Plateau : le plateau généré)
tirer_plateau(Liste_tuile, Plateau):-tirer_plateau(Liste_tuile, Plateau, 0).
tirer_plateau(_, Plateau, N):- N>=4, Plateau = [], !.
tirer_plateau(Liste_tuile, Plateau, N):- N<4, N_prime is N+1, tirer_ligne(Liste_tuile, Ligne, Nouv_liste), tirer_plateau(Nouv_liste, X, N_prime), Plateau = [Ligne|X].

% Génère le plateau de départ à partir de la liste de tuile prédéfinie par le jeu (-Plateau)
plateau_depart(Plateau):-tuile(X), tirer_plateau(X, Plateau).

% Vérifie qu'un coup est possible (ne peut générer qu'un seul coup) par rapport à un plateau et le dernier coup joué (+Plateau, +Ancienne_tuile, ?Nouveau_coup)
coup_possible(_,[],[_,[S,V]]):-sujet(S),vegetation(V),!.
coup_possible(P,[Fig,_],[_,[Fig,Q]]):-sujet(Fig),vegetation(Q),element([Fig,Q],P),!.
coup_possible(P,[_,Veg],[_,[T,Veg]]):-sujet(T),vegetation(Veg),element([T,Veg],P).

% Génère tous les coups possibles pour un plateau donné, en utilisant le coup précédent (+Plateau, +Ancienne_tuile, ?Nouveaux_coups : les coups possibles retournés un par un).
coup_possible2(P,[],[_,[C1,C2]]):-sujet(C1),vegetation(C2),element([C1,C2],P).
coup_possible2(P,[Fig1,_],[_,[Fig1,Veg2]]):-sujet(Fig1),vegetation(Veg2),element([Fig1,Veg2],P).
coup_possible2(P,[_,Veg1],[_,[Fig2,Veg1]]):-sujet(Fig2),vegetation(Veg1),element([Fig2,Veg1],P).

% Retourne une liste contenant tous les coups possibles pour un état donné (+P plateau, +DerT dernière tuile jouée, -Liste résultat)
liste_coups_possibles(P,DerT,Liste):-findall(X,coup_possible2(P, DerT,[_|[X]]), Liste).

% Remplace un élément par un autre dans une ligne du plateau (+T1 : élément à remplacer, +T2 : élément à insérer, +L1 : liste de départ, -L2 : résultat).
remplacer_dans_ligne(T1,T2,[T1|Q],[T2|Q]):-!.
remplacer_dans_ligne(T1,T2,[X|Q],[X|Temp]):-remplacer_dans_ligne(T1,T2,Q,Temp).

% Remplace un élément par un autre dans un plateau (+T1 : élément à remplacer, +T2 : élément à insérer, +P1 : plateau de départ, -P2 : nouveau plateau).
remplacer_dans_plateau(T1,T2,[L1|Q],[L2|Q]):-member(T1,L1),remplacer_dans_ligne(T1,T2,L1,L2),!.
remplacer_dans_plateau(T1,T2,[L1|Q],[L1|Y]):-remplacer_dans_plateau(T1,T2,Q,Y).

% Retourne le nouveau plateau après qu un joueur ait joué (+PI : le plateau initial, +[J,T] : Coup à jouer, -NP : Nouveau Plateau)
jouer_coup(PI,[J,T],NP):-remplacer_dans_plateau(T,J,PI,NP).

% Divise une liste en deux sous liste selon un indice N. L'élément à l'indice N n'est pas présent dans les listes de retour(+L : Liste de départ, +N : indice où séparer la liste, -L1 : liste de la partie à gauche de N, -L2 : liste de la partie à droite de N)
divise_liste(L,N,L1,L2):-divise_liste(L,N,L1,L2,1).
divise_liste([],_,[],[],_).
divise_liste([_|Q],N,L1,L2,M):-M=:=N,M_prime is M+1, divise_liste(Q,N,L1,L2,M_prime).
divise_liste([T|Q],N,[T|Q1],L2,M):-M<N, M_prime is M+1, divise_liste(Q,N,Q1,L2,M_prime).
divise_liste([T|Q],N,L1,[T|Q2],M):-M>N, M_prime is M+1, divise_liste(Q,N,L1,Q2,M_prime).

% Demande à un utilisateur de rentrer une tuile au format "Sujet-Vegetation" (ex:"So-Pi") (-T : la tuile que l'utilisateur a entré sous forme de liste de constante)
lire_tuile(T):-write('Tuile ("Sujet-Vegetation") : '),read(X), nth(N,X,45), divise_liste(X,N,L1,L2), name(Sujet,L1), convertir(Sujet,T1), name(Vegetation,L2), convertir(Vegetation,T2), T = [T1,T2],!.

% Vrai si le plateau est gagnant en ligne (4 pions alignés) (+Plateau)
gagner_ligne(P,Gagnant):-member([Gagnant,Gagnant,Gagnant,Gagnant], P),!.

% Vrai si le plateau est gagnant en colonnes (4 pions alignés verticalement) (+Plateau)
gagner_colonne(P,Gagnant):-colonnes(P,Cols),gagner_ligne(Cols,Gagnant).

% Vrai si le plateau contient une diagonale de pion (+Plateau)
gagner_diagonale(P,T):-diagonale1(P,[T|Q]), gagner_diagonale_rec(Q,T),!.
gagner_diagonale(P,T):-diagonale2(P,[T|Q]), gagner_diagonale_rec(Q,T).
gagner_diagonale_rec([T|Q],T):-gagner_diagonale_rec(Q,T).
gagner_diagonale_rec([],_).

% Vrai si le carré est composé de quatre tuiles identiques (+Carré)
carre_gagnant([T|Q],T):-carre_gagnant(Q,T).
carre_gagnant([],_).

% Vrai si le plateau contient un carré 4*4 de pion (+Plateau)
gagner_carre(P,Vain):-carres(P, Carres), gagner_carre_rec(Carres,Vain).
gagner_carre_rec([T|_],Vain):-carre_gagnant(T,Vain),!.
gagner_carre_rec([_|Q],Vain):-gagner_carre_rec(Q,Vain).

% Retourne l'autre joueur (+J1 joueur actuel, -J2 autre joueur).
autre_joueur(j1,j2).
autre_joueur(j2,j1).

% Vrai si le plateau est gagnant (Si une ligne, une colonne, un carré ou une diagonale est rempli par les pions d'un joueur ou qu'aucun coup n'est possible) (+Plateau, +Vain : le joueur courant, +Der_tuile : la dernière tuile jouée)
victoire(P,Vain,_):-gagner_ligne(P,Vain),!.
victoire(P,Vain,_):-gagner_colonne(P,Vain),!.
victoire(P,Vain,_):-gagner_diagonale(P,Vain),!.
victoire(P,Vain,_):-gagner_carre(P,Vain),!.
victoire(P,_,Der_tuile):-liste_coups_possibles(P,Der_tuile,[]).


% % % % % % % % % 
% Boucle de jeu %
% % % % % % % % %

% Retourne le message en cas de victoire (+J le joueur).
message_victoire(J):-nl, write('***********************'), nl, write('***** '), write(J), write(' A GAGNE *****'), nl, write('***********************'), nl.

% Menu de sélection
jouer:-write('######## MENU ########'),nl,write('1 - Humain vs Humain'),nl,write('2 - Humain vs IA'),nl,write('3 - IA vs IA'),nl,write('######################'),nl,write('Choix : '),read(X),menu(X).
menu(1):-!,nl,write('%%%%%%%%%%%%%%%%%%%%%%%'),nl,write('% Nouvelle partie PVP %'),nl,write('%%%%%%%%%%%%%%%%%%%%%%%'),nl,nl,plateau_depart(P),jouer_humains(P,j1,[]).
menu(2):-!,nl,write('%%%%%%%%%%%%%%%%%%%%%%%'),nl,write('% Nouvelle partie PVE %'),nl,write('%%%%%%%%%%%%%%%%%%%%%%%'),nl,nl,plateau_depart(P),jouer_vs_ia(P).
menu(3):-!,nl,write('%%%%%%%%%%%%%%%%%%%%%%%'),nl,write('% Nouvelle partie EVE %'),nl,write('%%%%%%%%%%%%%%%%%%%%%%%'),nl,nl,plateau_depart(P),jouer_deux_ia(P,j1,[]).
menu(_):-nl,write('/!\\ Entree non reconnue /!\\'),nl,jouer.

% Boucle de jeu : Affiche le plateau, le joueur en cours, la dernière tuile et gère les coups invalides. 
jouer_humains(P,J,Dt):-nl,jouer_humain(P,J,Dt,NP,T),(victoire(NP,J,T) -> 
				message_victoire(J),afficher_plateau(NP); 
				autre_joueur(J,J2), jouer_humains(NP,J2,T)).

jouer_humain(P,J,Dt,NP,T):-write('C\'est a '), write(J), write(' de jouer !'), nl,
	(Dt = [] -> 
		nl;
		write('Ancien coup : '),write(Dt),nl),
	afficher_plateau(P), nl, 
	(lire_tuile(Tu) ->  
		(coup_possible(P,Dt,[J,Tu]) -> 
			T=Tu, jouer_coup(P,[J,Tu],P2),NP=P2;
			write('/!\\ Coup non valide /!\\'), nl, jouer_humain(P,J,Dt,Q,V),NP=Q,T=V);
		write('/!\\ Coup non reconnu /!\\'), nl, nl, jouer_humain(P,J,Dt,Q,V), NP=Q,T=V).
		
jouer_ia(P,J,Dt,NP,T):-write('C\'est a '), write(J), write(' de jouer !'), nl,
	(Dt = [] -> 
		nl;
		write('Ancien coup : '),write(Dt),nl),
	afficher_plateau(P), nl, 
	meilleur_coup(P, Dt, J, T), jouer_coup(P,[J,T],NP).

% jouer_vs_ia permet de choisir quel sera le premier joueur (l'IA ou le joueur humain) de façon aléatoire
jouer_vs_ia(P):-randomize, random(1, 3, R), R=:=1 -> write('Le joueur humain commence :'),nl,jouer_vs_ia1(P,j1,[]); write('L''intelligence artificielle commence :'),nl, jouer_vs_ia2(P,j1,[]).
jouer_vs_ia1(P,J,Dt):-nl,jouer_humain(P,J,Dt,NP,T),(victoire(NP,J,T) -> 
				message_victoire(J),afficher_plateau(NP); 
				autre_joueur(J,J2), nl,jouer_ia(NP,J2,T,NP2,T2),(victoire(NP2,J2,T2) -> 
					message_victoire(J2),afficher_plateau(NP2); 
					jouer_vs_ia1(NP2,J,T2))).
					
jouer_vs_ia2(P,J,Dt):-nl,jouer_ia(P,J,Dt,NP,T),(victoire(NP,J,T) -> 
				message_victoire(J),afficher_plateau(NP); 
				autre_joueur(J,J2), nl,jouer_humain(NP,J2,T,NP2,T2),(victoire(NP2,J2,T2) -> 
					message_victoire(J2),afficher_plateau(NP2); 
					jouer_vs_ia2(NP2,J,T2))).
		
jouer_deux_ia(P,J,Dt):-nl, jouer_ia(P,J,Dt,NP,T),(victoire(NP,J,T) -> 
					message_victoire(J),afficher_plateau(NP); 
					autre_joueur(J,J2),read(_), jouer_deux_ia(NP,J2,T)).

					test:-randomize, random(1, 3, R),write(R).
