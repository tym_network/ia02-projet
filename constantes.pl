% % % % % % % %
% CONSTANTES  %
% % % % % % % %

% Retourne la liste de toutes les tuiles possibles (-Tuiles)
tuile([
	[oiseau, erable],[tanzaku,iris] ,[soleil,erable], [pluie,cerisier],	[tanzaku,pin], [oiseau,pin], [pluie,erable], [soleil,iris],
	[pluie,iris], [pluie, pin], [soleil,cerisier], [oiseau,cerisier],
	[soleil,pin], [oiseau,iris], [tanzaku,cerisier], [tanzaku,erable]
]).

% Vrai si X est un sujet (+X : le sujet)
sujet(oiseau).
sujet(tanzaku).
sujet(soleil).
sujet(pluie).

% Vrai si X est une végétation (+X : la végétation)
vegetation(erable).
vegetation(iris).
vegetation(pin).
vegetation(cerisier).

% Convertit une chaine en sa constante (+S : une chaine, -X : la constante associée)
convertir('Oi',X):-X = oiseau.
convertir('Ta',X):-X = tanzaku.
convertir('So',X):-X = soleil.
convertir('Pl',X):-X = pluie.
convertir('Er',X):-X = erable.
convertir('Ir',X):-X = iris.
convertir('Ce',X):-X = cerisier.
convertir('Pi',X):-X = pin.

