% % % % % % %
% AFFICHAGE %
% % % % % % %

% Affiche une constante (sujet ou végétation) sur deux caractères (+X : une constante du jeu)
afficher_theme(oiseau):-write('Oi').
afficher_theme(tanzaku):-write('Ta').
afficher_theme(pluie):-write('Pl').
afficher_theme(soleil):-write('So').
afficher_theme(erable):-write('Er').
afficher_theme(iris):-write('Ir').
afficher_theme(cerisier):-write('Ce').
afficher_theme(pin):-write('Pi').

% Affiche une tuile (= une case du plateau) (+Tuile)
afficher_case([X,Y]):-afficher_theme(X),write('  '), afficher_theme(Y),!.
afficher_case(X):-write('  '), write(X), write('  ').

% Affiche une ligne du plateau (+Ligne : une liste de cases)
afficher_ligne([]):-write('\n'),write('-----------------------------'),write('\n').
afficher_ligne([X|Y]):-afficher_case(X), write('|'), afficher_ligne(Y).

% Affiche le plateau complet (+Plateau)
afficher_plateau([]).
afficher_plateau([X|Y]):-write('-----------------------------'), nl, afficher_plateau_rec([X|Y]).
afficher_plateau_rec([]).
afficher_plateau_rec([X|Y]):-write('|'), afficher_ligne(X), afficher_plateau_rec(Y).

% Obtenir la liste des résultats : findall(+var,predicat,listeDesValeurs),bagof, setof. 
% Si pas de résultat : findall renvoie liste vide, les autres faux.
% Bagof : tous les résultats y compris les doublons, sans ordre.
% Setof : pas de doublons, trié
