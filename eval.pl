% % % % % % % % % % % % % %
% FONCTIONS D'EVALUATION  %
% % % % % % % % % % % % % %

% Retourne 0 si un pion adverse est sur la diagonale 1 ou nombre de pion du joueur +1 si il n'y a pas de pion adverse(+Plateau, +J joueur, -N)
in_diagonale1(Plateau,J,0):-diagonale1(Plateau, D1), autre_joueur(J,J2), member(J2,D1),!.
in_diagonale1(Plateau,J,N):-diagonale1(Plateau, D1), nb_occur(J,D1,N2), N is N2+1.

% Retourne 0 si un pion adverse est sur la diagonale 2 ou nombre de pion du joueur +1 si il n'y a pas de pion adverse (+Plateau, +J joueur, -N)
in_diagonale2(Plateau,J,0):-diagonale2(Plateau, D2), autre_joueur(J,J2), member(J2,D2),!.
in_diagonale2(Plateau,J,N):-diagonale2(Plateau, D2), nb_occur(J,D2,N2), N is N2+1.

% Renvoi le score des lignes (somme du score de chaque ligne). Pour chaque ligne : 0 si un pion adverse est présent, 1 si la ligne est vide, nb occurences+1 pour chaque pion du joueur (+Plateau, +J : le joueur, -N : le score des lignes)
ligne_possible([T|Q],J,N):-autre_joueur(J,J2), member(J2,T), !, ligne_possible(Q,J,N).
ligne_possible([T|Q],J,N):-autre_joueur(J,J2), \+member(J2,T), nb_occur(J,T,N3), ligne_possible(Q,J,N2), N is N2+N3+1. 
ligne_possible([],_,0).

% Renvoi le score des colonnes (somme du score de chaque colonne). Pour chaque colonne : 0 si un pion adverse est présent, 1 si la colonne est vide, nb occurences+1 pour chaque pion du joueur (+Plateau, +J : le joueur, -N : le score des colonnes)
colonne_possible(Plateau,J,N):-colonnes(Plateau,NP), ligne_possible(NP,J,N).

% Renvoi le score des diagonales (somme du score de chaque diagonale). Pour chaque diagonale : 0 si un pion adverse est présent, 1 si la diagonale est vide, nb occurences+1 pour chaque pion du joueur (+Plateau, +J : le joueur, -N le score des diagonales)
diagonale_possible(Plateau,J,N):-in_diagonale1(Plateau,J,N1),in_diagonale2(Plateau,J,N2), N is N1+N2 .

% Renvoi le score des carrés 2*2 du plateau (somme du score de chaque carré). Pour chaque carré : 0 si un pion adverse est présent, 1 sans aucun pion, nb occurences+1 pour chaque pion du joueur (+Plateau, +J : le joueur, -N le score des carrés)
carre_possible(Plateau,J,N):-carres(Plateau,Liste),evalue_carre(Liste,J,N).
evalue_carre([T|Q], J, N):- autre_joueur(J,J2), member(J2, T),!, evalue_carre(Q, J ,N).
evalue_carre([T|Q], J, N):- nb_occur(J,T,N3), evalue_carre(Q, J ,N2), N is N2+N3+1.
evalue_carre([],_,0).

% Renvoie le nombre de d'occurences de l'élément recherché dans la liste (+[T|Q] : la liste, +J : l'élément, -N le nombre d'occurences
occurences_dans_liste([T|Q],J,N):-occurences_dans_liste(Q,J,N2),(member(J,T)->N is N2;N is N2+1).
occurences_dans_liste([],_,0).

% Renvoie une note indiquant les chances de victoires brutes d'un joueur pour un plateau donné (+P : le plateau, +J : le joueur, -N : le score) 
score_brut(P,J,Tuile_prec,N):-victoire(P,J,Tuile_prec),!,N=10000.
score_brut(P,J,_,N):-ligne_possible(P,J,N1), colonne_possible(P,J,N2),diagonale_possible(P,J,N3),carre_possible(P,J,N4), N is N1+N2+N3+N4.

% Renvoie une note indiquant les chances de victoire d'un joueur en tenant compte des possibilités de l'adversaire(+P : le plateau, +J : le joueur, -N : le score).
% Nous sommes obligé de retester si la liste des coups possibles est nulle car la victoire ne prend pas en compte le joueur dans ce cas. Par conséquent les 10000 points attribués par la victoire s'annuleraient
score_net(P,J,Tuile_prec,N):-score_brut(P,J,Tuile_prec,N1),(liste_coups_possibles(P,Tuile_prec,[])->N3 is N1+10000;N3 is N1),autre_joueur(J,J2),score_brut(P,J2,Tuile_prec,N2), N is N3 - N2.
