% % % % % % % % % % % % % %
% MANIPULATION DU PLATEAU %
% % % % % % % % % % % % % %

% Vrai si le couple X est présent sur le plateau, faux sinon (+X : l'élément cherché, +Plateau).
element(X,[T|_]):-member(X,T).
element(X,[_|Q]):-element(X,Q).

% Retourne la taille du plateau (supposé carré) (+Plateau, -Dim : dimensions du plateau)
dimension(Plateau, Dim):-length(Plateau, Dim).

% Retourne le nombre d'occurences de X dans la liste L; 0 si aucune occurence (+X : l'élément recherché, +L : la liste dans laquelle chercher, -N : le nombre d'occurences)
nb_occur(X,L,N):-findall(X, member(X, L), Liste), length(Liste, N).

% Retourne l'élément N de la liste (+N indice de l'élément à retourner, +Liste la liste dans laquelle rechercher, -E l'élément)
element_n(1,[T|_],T):-!.
element_n(N,[_|Q],X):-N>1, N2 is N-1, element_n(N2,Q,X).

% Retourne la colonne N du plateau (+N l'indice de la colonne, +Plateau, -Colonne)
colonne_n(_, [], []):-!.
colonne_n(N,[T|Q],[C1|C2]):-element_n(N,T,C1), colonne_n(N,Q,C2).

% Retourne le carré 2*2 à partir de l'élément situé à la ligne M, colonne N (+M : ligne du coin supérieur gauche du carré, +N : colonne du coin supérieur gauche du carré, +Plateau, -Carre : Liste des quatres tuiles du carré)
carre_n(M,N,Plateau,Carre):-dimension(Plateau,D), M<D, N<D, N2 is N+1, M2 is M+1, element_n(M,Plateau,Ligne1), element_n(N,Ligne1,E1), element_n(N2,Ligne1,E2), element_n(M2,Plateau,Ligne2), element_n(N,Ligne2,E3), element_n(N2,Ligne2,E4), append([E1],[E2,E3|[E4]],Carre),!.
carre_n(_,_,_,[]).

% Retourne la diagonale du haut gauche au bas droit (+Plateau, -D1 : la diagonale sous forme de liste)
diagonale1(Plateau,D1):-diagonale1(1,Plateau,D1),!.
diagonale1(I,Plateau,D1):-dimension(Plateau,Dim), I=<Dim, I2 is I+1, element_n(I,Plateau,Ligne), element_n(I, Ligne, X), diagonale1(I2,Plateau,Res),D1=[X|Res].
diagonale1(I,Plateau,[]):-dimension(Plateau,Dim), I>Dim.

% Retourne la diagonale du haut droit au bas gauche (+Plateau, -D2 : la diagonale sous forme de liste)
diagonale2(Plateau,D2):-diagonale2(1,Plateau,D2),!.
diagonale2(J,Plateau,D2):-dimension(Plateau,Dim), J=<Dim, J2 is J+1, Ji is (Dim-J+1), element_n(J,Plateau,Ligne), element_n(Ji, Ligne, X), diagonale2(J2,Plateau,Res),D2=[X|Res].
diagonale2(J,Plateau,[]):-dimension(Plateau,Dim), J>Dim.

% Retourne le plateau comme une liste de colonnes (+Plateau, -Cs : Liste de colonnes)
colonnes(Plateau,Cs):-colonnes(1,Plateau,Cs),!.
colonnes(N,Plateau,Cs):-dimension(Plateau,D), N=<D, N2 is N+1, colonne_n(N,Plateau,Col), colonnes(N2,Plateau,Res), Cs=[Col|Res].
colonnes(N,Plateau,[]):-dimension(Plateau,D), N>D.

% Retourne l'ensemble de tous les carrés du plateau dans une liste (+Plateau, -Liste des carrés)
carres(Plateau,Liste):-dimension(Plateau,D),D2 is D-1,carres(D2,D2,Plateau,Liste).
carres(0,_,_,[]):-!.
carres(M,0,Plateau,Liste):-M>0, !, M2 is M-1, dimension(Plateau,D), D2 is D-1, carres(M2,D2,Plateau,Liste).
carres(M,N,Plateau,[T|Q]):-M>0,N>0, N2 is N-1, carre_n(M,N,Plateau,T),carres(M,N2,Plateau,Q).

